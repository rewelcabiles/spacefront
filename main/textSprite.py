import pygame


class FontSprite(pygame.sprite.Sprite):
    def __init__(self, name, text, coords):
        pygame.sprite.Sprite.__init__(self)
        self.name = name
        self.image = text
        self.rect = self.image.get_rect()
        self.rect.x = coords[0]
        self.rect.y = coords[1]

