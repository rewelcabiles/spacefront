import pygame
import config_loader
import StackEngine

def main():
    pygame.init()
    #Read Config Files and apply them.
    configs = config_loader.ConfigEditor()
    videoSettings = configs.video_config_load()
    fsFlags = False
    if videoSettings["fullscreen"] == True:
        fsFlags = pygame.FULLSCREEN | pygame.HWSURFACE
    else:
        fsFlags = False
    screen = pygame.display.set_mode(videoSettings["resolution"], fsFlags)
    #Game loop constants
    clock = pygame.time.Clock()
    stackManager = StackEngine.StackManager(screen)
    gameRunning = True
    font = pygame.font.SysFont("monospace", 20)
    screensize = screen.get_size()
    screeners = pygame.Rect((0,0),screensize)
    while gameRunning:
        current_time = clock.tick_busy_loop() #Currently limits the game to 260fps, will change soon
        fps = clock.get_fps() #Get current Fps
        dt = current_time / 1000.0
        stackManager.update_render(dt)
        screen.blit(font.render(str(fps), 1, (255,255,255)),(0,0))
        pygame.display.update(screeners)
if __name__ == "__main__":
    main()