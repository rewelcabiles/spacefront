import pygame
import Stacks


class StackManager:
    def __init__(self, old_screen):
        self.screen = old_screen
        self.Stacks = Stacks.Stacks(self.screen)
        self.currentWindow = self.Stacks.title_screen(self.screen)

    def update_render(self, dt):
        self.update(dt)
        self.render()
        self.handle_events()

    def update(self, dt):
        self.currentWindow.update(dt)
        if self.currentWindow.done:
            self.currentWindow = self.Stacks.stateDict[self.currentWindow.nextState](self.screen)
            self.screen.fill((255, 255, 255))

    def render(self):
        self.currentWindow.render()

    def handle_events(self):
        events = pygame.event.get()
        self.currentWindow.handle_events(events)
        for event_type in events:
            if event_type.type == pygame.KEYDOWN:
                if event_type.key == pygame.K_ESCAPE:
                    pygame.quit()
