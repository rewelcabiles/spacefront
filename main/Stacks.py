from MainMenu import TitleScreen
from GameStates import GameStackEngine


class Stacks:
    def __init__(self, screen):
        self.title_screen = TitleScreen.TitleStack
        self.game = GameStackEngine.GameStacks
        self.stateDict={
            "title": self.title_screen,
            "game": self.game
        }