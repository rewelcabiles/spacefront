import menuStacks


class TitleStack:
    def __init__(self, screen):
        self.screen = screen
        self.done = False
        self.nextState = "game"
        self.stacker = menuStacks.MenuStacks()
        self.goto(self.stacker.Title(self.screen))

    def goto(self, nextScene):
        self.currentWindow = nextScene
        self.currentWindow.StackHelper = self

    def update(self, dt):
        self.currentWindow.update()

    def render(self):
        self.currentWindow.render()

    def handle_events(self, events):
        if self.currentWindow.handle_events(events) == True:
            self.done = True