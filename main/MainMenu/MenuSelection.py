import pygame
import menuStacks
from main import textSprite


class Options:
    def __init__(self, screen):
        self.screen = screen
        self.renderItems = []
        self.stacker = menuStacks.MenuStacks()
        self.load_stuff()
        self.renderItems.append(self.background1)
        self.font = pygame.font.Font("assets/fonts/pkmnrsi.ttf", 90)
        self.optionList = pygame.sprite.Group()
        self.create_options()
        self.nextState = None

    def load_stuff(self):
        background1 = (pygame.image.load("assets/images/backdrops/titlescreen.png").convert(), (0, 0))
        self.background1 = (pygame.transform.scale(background1[0], (self.screen.get_size()) ) , (0, 0))

    def state_change(self):
        return self.nextState

    def create_options(self):
        pos = self.screen.get_size()
        self.button_play = textSprite.FontSprite("play", self.font.render("NEW GAME", 1, [255, 255, 0]), (pos[0] * 0.45 - 80, pos[1] * 0.5 - 52))
        self.button_options = textSprite.FontSprite("options", self.font.render("OPTIONS", 1, [255, 255, 0]), (pos[0] * 0.44 - 50, pos[1] * 0.65 - 52))
        self.optionList.add(self.button_play)
        self.optionList.add(self.button_options)

        selector = pygame.Surface((24, 24))
        selector.fill((255,255,0))
        self.selection = textSprite.FontSprite("selection", selector, (-20, 0))
        self.optionList.add(self.selection)

        self.currentNumber = 0
        self.currentSelection = self.button_play
        self.selectionList = [
            self.button_play,
            self.button_options
        ]
        self.get_new_selection()

    def get_new_selection(self):
        self.selection.rect.y = self.currentSelection.rect.y + 23
        self.selection.rect.x = self.currentSelection.rect.x - 40

    def handle_buttons(self, button, pressed):
        if pressed:
            if self.currentSelection.name == "options":
                self.StackHelper.goto(self.stacker.Options(self.screen))
            elif self.currentSelection.name == "play":
                return True

        else:
            if button.name == "play":
                self.currentSelection = self.button_play
            elif button.name == "options":
                self.currentSelection = self.button_options
            self.get_new_selection()

    def update(self):
        pass

    def render(self):
        for blitable in self.renderItems:
            self.screen.blit(blitable[0], blitable[1])
        self.optionList.draw(self.screen)

    def handle_events(self, events):
        mousepos = pygame.mouse.get_pos()
        for buttons in self.optionList:
            if buttons.rect.collidepoint(mousepos):
                self.handle_buttons(buttons, False)
        for event_type in events:
            if event_type.type == pygame.KEYDOWN :
                if(event_type.key == pygame.K_DOWN) or (event_type.key == pygame.K_s):
                    try:
                        self.currentNumber += 1
                        self.currentSelection = self.selectionList[self.currentNumber]

                    except IndexError:
                        self.currentNumber -= 1
                    self.get_new_selection()
                elif(event_type.key == pygame.K_UP) or (event_type.key == pygame.K_w):
                    try:
                        self.currentNumber -= 1
                        self.currentSelection = self.selectionList[self.currentNumber]
                    except IndexError:
                        self.currentNumber += 1
                    self.get_new_selection()
                elif event_type.key == pygame.K_RETURN:
                    if self.handle_buttons(self.currentSelection, True):
                        return True

            if event_type.type == pygame.MOUSEBUTTONDOWN:
                mousepos = pygame.mouse.get_pos()
                for buttons in self.optionList:
                    if buttons.rect.collidepoint(mousepos):
                        if self.handle_buttons(buttons, True):
                            return True

