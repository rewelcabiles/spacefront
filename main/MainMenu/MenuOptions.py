import pygame
import menuStacks
from main import textSprite
from main import config_loader


class VidOptions:
    def __init__(self, screen):
        self.screen = screen
        self.renderItems = []
        self.stacker = menuStacks.MenuStacks()
        self.optionList = pygame.sprite.Group()
        self.nonButtonList = pygame.sprite.Group()
        self.font = pygame.font.Font("assets/fonts/pkmnrsi.ttf", 90)

        self.config = config_loader.ConfigEditor()
        self.videoDict = self.config.video_config_load()

        self.load_stuff()
        self.create_options()
        self.nextState = None

        self.currentResolution = 1
        self.currentFullscreen = 1

        self.resolutionList = [
            (1360, 765),
            (800, 450)
        ]
        self.fullscreenList = [
            "True", "False"
        ]

    def load_stuff(self):
        background1 = (pygame.image.load("assets/images/backdrops/titlescreen.png").convert(), (0, 0))
        self.background1 = (pygame.transform.scale(background1[0], (self.screen.get_size()) ) , (0, 0))
        self.renderItems.append(self.background1)

    def create_options(self):
        pos = self.screen.get_size()

        self.button_resolution = textSprite.FontSprite("resolution", self.font.render("RESOLUTION: ", 1, [255, 255, 0]), (pos[0] * 0.24 - 80, pos[1] * 0.50 - 52))
        self.button_fullscreen = textSprite.FontSprite("fullscreen", self.font.render("FULLSCREEN: ", 1, [255, 255, 0]), (pos[0] * 0.24 - 50, pos[1] * 0.65 - 52))
        self.backButton = textSprite.FontSprite("leave", self.font.render("<BACK> ", 1, [255, 255, 0]), (pos[0] * 0.24 - 40, pos[1] * 0.80 - 52))
        self.text_resolution = textSprite.FontSprite("resolutionValue", self.font.render(str(self.videoDict["resolution"]), 1, [255, 255, 0]), (self.button_resolution.rect.x + self.button_resolution.rect.width + 20, self.button_resolution.rect.y))
        self.text_fullscreen = textSprite.FontSprite("fullscreenValue", self.font.render(str(self.videoDict["fullscreen"]), 1, [255, 255, 0]), (self.button_fullscreen.rect.x + self.button_fullscreen.rect.width + 20, self.button_fullscreen.rect.y))

        self.nonButtonList.add(self.text_resolution)
        self.nonButtonList.add(self.text_fullscreen)

        self.optionList.add(self.backButton)
        self.optionList.add(self.button_resolution)
        self.optionList.add(self.button_fullscreen)

        selector = pygame.Surface((24, 24))
        selector.fill((255,255,0))
        self.selection = textSprite.FontSprite("selection", selector, (-20, 0))
        self.optionList.add(self.selection)

        self.currentNumber = 0
        self.currentSelection = self.button_resolution
        self.selectionList = [
            self.button_resolution,
            self.button_fullscreen,
            self.backButton
        ]
        self.get_new_selection()

    def writeToInit(self):
        self.config.video_config_writer(self.resolutionList[self.currentResolution], self.fullscreenList[self.currentFullscreen])

    def get_new_selection(self):
        self.selection.rect.y = self.currentSelection.rect.y + 23
        self.selection.rect.x = self.currentSelection.rect.x - 40

    def state_change(self):
        return self.nextState

    def handle_buttons(self, button, pressed):
        if pressed:
            self.toggleButtons()

        else:
            if button.name == "leave":
                self.currentSelection = self.backButton
            elif button.name == "resolution":
                self.currentSelection = self.button_resolution
            elif button.name == "fullscreen":
                self.currentSelection = self.button_fullscreen
            self.get_new_selection()

    def updateText(self):
        self.text_resolution.image = self.font.render(str(self.videoDict["resolution"]), 1, (255,255,0), (self.button_resolution.rect.x + self.button_resolution.rect.width + 20, self.button_resolution.rect.y))
        self.text_fullscreen.image = self.font.render(str(self.videoDict["fullscreen"]), 1, (255,255,0), (self.button_fullscreen.rect.x + self.button_fullscreen.rect.width + 20, self.button_fullscreen.rect.y))

    def update(self):
        pass

    def toggleButtons(self):
        if self.currentSelection.name == "leave":
            self.writeToInit()
            self.StackHelper.goto(self.stacker.Title(self.screen))
        elif self.currentSelection.name == "resolution":
            if self.currentResolution == 1:
                self.currentResolution = 0
            else:
                self.currentResolution = 1
            self.videoDict["resolution"] = self.resolutionList[self.currentResolution]
        elif self.currentSelection.name == "fullscreen":
            if self.currentFullscreen == 1:
                self.currentFullscreen = 0
            else:
                self.currentFullscreen = 1
            self.videoDict["fullscreen"] = self.fullscreenList[self.currentFullscreen]
        self.updateText()

    def render(self):
        for blitable in self.renderItems:
            self.screen.blit(blitable[0], blitable[1])

        self.nonButtonList.draw(self.screen)
        self.optionList.draw(self.screen)

    def handle_events(self,events):
        mousepos = pygame.mouse.get_pos()
        for buttons in self.optionList:
            if buttons.rect.collidepoint(mousepos):
                self.handle_buttons(buttons, False)

        for event_type in events:
            if event_type.type == pygame.KEYDOWN :
                if(event_type.key == pygame.K_DOWN) or (event_type.key == pygame.K_s):
                    try:
                        self.currentNumber += 1
                        self.currentSelection = self.selectionList[self.currentNumber]

                    except IndexError:
                        self.currentNumber -= 1
                    print self.currentSelection.name
                    self.get_new_selection()
                elif(event_type.key == pygame.K_UP) or (event_type.key == pygame.K_w):
                    try:
                        self.currentNumber -= 1
                        self.currentSelection = self.selectionList[self.currentNumber]
                    except IndexError:
                        self.currentNumber += 1
                    print self.currentSelection.name
                    self.get_new_selection()
                elif event_type.key == pygame.K_RETURN:
                    self.handle_buttons(self.currentSelection, True)
            if event_type.type == pygame.MOUSEBUTTONDOWN:
                mousepos = pygame.mouse.get_pos()
                for buttons in self.optionList:
                    if buttons.rect.collidepoint(mousepos):
                        self.handle_buttons(buttons, True)
