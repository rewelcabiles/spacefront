import pygame

from main.GameStates.ShipParts import BasicComponents
from main.GameStates.ShipParts import HullParts
from main.GameStates.ShipParts import weaponParts

class Player:
    def __init__(self):
        self.name = "Test Subject"
        self.Ship = Ship()


class Ship:
    def __init__(self):
        self.Hull = HullParts.FederationStandard()
        self.stats = self.Hull.stats
        self.handleEvents = ControllerInputs()
        self.movement = BasicComponents.Movement(self.stats)
        self.ShipPartImages = (self.Hull.AllSpriteComponents)
        self.Rotation = BasicComponents.Rotate(self.Hull)

    def update(self, dt):
        self.ShipPartImages = self.Hull.AllSpriteComponents
        self.States = self.handleEvents.getStates()
        self.Hull.update(self.States)
        self.movement.update(self.States)
        self.Rotation.update(dt)

    def events(self, events):
        self.handleEvents.Event_Handle(events)


class ControllerInputs:
    def __init__(self):
        self.currentState = None
        self.ShipStates = {
            "left": False,
            "right": False,
            "forward": False,
            "back" : False,
            "damage": False,
            "Slot1": False,
            "Slot2": False,
            "click": False
        }

    def getStates(self):
        return self.ShipStates

    def Movement(self, events):
        self.isPressed = pygame.key.get_pressed()
        self.mPressed = pygame.mouse.get_pressed()

        if self.isPressed[pygame.K_a]:
            self.ShipStates["left"] = True
        else:
            self.ShipStates["left"] = False

        if self.isPressed[pygame.K_d]:
            self.ShipStates["right"] = True
        else:
            self.ShipStates["right"] = False

        if self.isPressed[pygame.K_w]:
            self.ShipStates["forward"] = True
        else:
            self.ShipStates["forward"] = False

        if self.isPressed[pygame.K_s]:
            self.ShipStates["back"] = True
        else:
            self.ShipStates["back"] = False

        if self.isPressed[pygame.K_f]:
            self.ShipStates["damage"] = True
        else:
            self.ShipStates["damage"] = False

        if self.isPressed[pygame.K_1]:
            self.ShipStates["Slot1"] = True
        else:
            self.ShipStates["Slot1"] = False

        if self.isPressed[pygame.K_2]:
            self.ShipStates["Slot2"] = True
        else:
            self.ShipStates["Slot2"] = False

        if self.mPressed[0]:
            self.ShipStates["click"] = True
        else:
            self.ShipStates["click"] = False

    def Event_Handle(self, events):
        if self.currentState is None:
            self.currentState = self.Movement
        self.currentState(events)



