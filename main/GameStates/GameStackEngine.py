import pygame
from GameStackList import GameStackList


class GameStacks:  # Includes the UI + the game Its A stack engine
    def __init__(self, screen):
        self.screen = screen
        self.done = False
        self.nextState = "title"
        self.StackList = GameStackList()
        self.StackStack = [
            self.StackList.OuterSpace(self.screen)
        ]

    def update(self, dt):
        self.StackStack[0].update(dt)

    def render(self):
        self.StackStack[0].render()

    def handle_events(self, events):
        self.StackStack[0].handle_events(events)
