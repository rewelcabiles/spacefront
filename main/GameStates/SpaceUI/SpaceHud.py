import pygame
from ActionBar import Actionbar

class SpaceHud:
    def __init__(self,screen, player):
        self.screen = screen
        self.ship = player.Ship
        self.AB = Actionbar(player.Ship.Hull.SlotManager)
        #self.health_bar = HealthBar()
        self.renderList = []
        self.spriteHud = pygame.sprite.Sprite()

    def update(self):
        self.renderList = []
        self.AB.update()
        #self.health_bar.update((self.ship.health.curHealth, self.ship.health.maxHealth))
        #self.renderList.append(self.health_bar.getBar())

    def render(self):
        self.AB.render(self.screen)
        for elements in self.renderList:
            self.screen.blit(elements[0], elements[1])

    def handle_input(self):
        pass

class ActionBar:
    def __init__(self):
        pass

class HealthBar:
    def update(self, health):
        self.curHealth = health[0]
        self.maxHealth = health[1]
        self.healthBarWidth = 500.0
        self.width = float(self.curHealth) / float(self.maxHealth) * self.healthBarWidth
        if self.width<=1:
            self.width=1
        self.HealthFrame = pygame.Surface((self.width, 30))
        self.HealthFrame.fill((0, 200, 255))

    def getBar(self):
        return (self.HealthFrame, (2, 2))

