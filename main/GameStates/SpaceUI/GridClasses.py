import pygame


class GridEmpty(pygame.sprite.Sprite):
    def __init__(self, gridSize):
        pygame.sprite.Sprite.__init__(self)
        self.ID = None
        self.empty = True
        self.image = pygame.Surface((gridSize, gridSize))
        self.image.fill((0, 100, 100))
        self.rect = self.image.get_rect()



class GridItem(pygame.sprite.Sprite):
    def __init__(self, image, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.ID = None
        self.image = pygame.image.load(image).convert_alpha()
        self.image_rect = self.image.get_rect()
        self.rect = pygame.Rect(0, 0, 52, 52)
        self.rect.x = x
        self.rect.y = y

    def follow_mouse(self, mousePos):
        self.rect.x = mousePos[0]
        self.rect.y = mousePos[1]

    def place_on_grid(self, pos):
        self.rect.x = pos[0]
        self.rect.y = pos[1]


class InventoryBar(pygame.sprite.Sprite):
    def __init__(self, name, pos):
        pygame.sprite.Sprite.__init__(self)
        self.name = name
        self.image = pygame.image.load("assets/images/menus/InventoryButton.png").convert()
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]


class DropDownRect(pygame.sprite.Sprite):
    def __init__(self, size, color, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((size))
        self.image.fill((color))
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]

