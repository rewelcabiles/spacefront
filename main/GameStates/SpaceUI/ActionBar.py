import pygame


class Actionbar:
    def __init__(self, playerslot):
        self.ActionBar = playerslot
        self.slotcount = len(self.ActionBar.SlotList)
        self.bar = createBar(self.ActionBar, 540)
        self.HotkeyList = self.bar.SlotGroup

        self.highlightONN = pygame.image.load("assets/images/menus/HotkeyHighlight.png").convert_alpha()
        self.highlightOFF = pygame.image.load("assets/images/menus/Transparent.png").convert_alpha()
        self.highlightOFF = pygame.transform.scale(self.highlightOFF, (42, 42))
        self.highlightONN = pygame.transform.scale(self.highlightONN, (42, 42))

        self.HighlightIcon = Hotkey(self.highlightOFF, None)
        self.HotkeyList.add(self.HighlightIcon)

    def update(self):

        for things in self.HotkeyList:
            if self.ActionBar.active_module == None:
                self.HighlightIcon.image = self.highlightOFF
            elif things.link is self.ActionBar.active_module:
                self.HighlightIcon.image = self.highlightONN
                self.HighlightIcon.rect.center = things.rect.center


    def render(self, screen):
        self.HotkeyList.draw(screen)

class createBar:
    def __init__(self, AB, x):
        self.Sprite = Hotkey
        self.ActionBar = AB
        self.SlotGroup = pygame.sprite.Group()
        self.initialX = x
        self.spacing = 43
        self.create()

    def create(self):
        for slots in self.ActionBar.SlotList:
            if self.ActionBar.SlotList[slots] is None:
                newKey = self.Sprite(pygame.image.load("assets/images/menus/HotkeyIcons.png"), None)
            else:
                newKey = self.Sprite(self.ActionBar.SlotList[slots].Sprite.icon, self.ActionBar.SlotList[slots])
            newKey.rect.x = self.initialX + self.spacing
            newKey.rect.y = 5
            self.spacing *= 2
            self.SlotGroup.add(newKey)


class Hotkey(pygame.sprite.Sprite):#Hotkey Picture things
    def __init__(self, image, link):
        pygame.sprite.Sprite.__init__(self)
        self.link = link
        self.image = image
        self.image = pygame.transform.scale(self.image, (42, 42))
        self.rect = self.image.get_rect()