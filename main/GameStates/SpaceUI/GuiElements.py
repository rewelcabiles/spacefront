import pygame

import main.GameStates.SpaceUI.GridClasses


class ShipEditGrids:
    def __init__(self, screen, col, rows, pos, gridSize):
        self.screen = screen
        self.start_pos_x = pos[0]
        self.start_pos_y = pos[1]
        self.gridSize = gridSize
        self.columns = col
        self.rows = rows
        self.gridList = pygame.sprite.Group()

    def create_grid(self):
        for rows in range(self.rows):
            for columns in range(self.columns):
                new_grid_piece = main.GameStates.SpaceUI.GridClasses.GridEmpty(self.gridSize)
                new_grid_piece.rect.x = self.start_pos_x + ((new_grid_piece.rect.width + 2) * columns)
                new_grid_piece.rect.y = self.start_pos_y + ((new_grid_piece.rect.height + 2) * rows)
                new_grid_piece.ID = (columns, rows)
                self.gridList.add(new_grid_piece)
        return self.gridList


class CargoMenu:
    def __init__(self, screen):
        self.screen = screen
        self.posX = self.screen.get_size()[0] * 0.75
        self.posY = self.screen.get_size()[1] * 0.04

        self.cargo_object_list = pygame.sprite.Group()
        self.drop_down_buttons = pygame.sprite.Group()

        self.InventoryButton = main.GameStates.SpaceUI.GridClasses.InventoryBar("toggle_inventory", (self.posX, 0))
        self.Dropdown = DropdownMenu(self.screen, (self.posX, 0 + 33))

        self.drop_down_buttons.add(self.InventoryButton)
        self.cargo_object_list.add(self.InventoryButton)

    def update(self):
        pass

    def render(self):
        self.cargo_object_list.draw(self.screen)
        self.Dropdown.render()

    def handle_events(self, events):
        for event_type in events:
            if event_type.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                for buttons in self.cargo_object_list:
                    if buttons.rect.collidepoint(mouse_pos):
                        self.buttonPressed(buttons)

    def buttonPressed(self, buttons):
        if self.drop_down_buttons.has(buttons):
            if buttons.name == "toggle_inventory":
                if self.Dropdown.visible == False:
                    self.Dropdown.visible = True
                elif self.Dropdown.visible == True:
                    self.Dropdown.visible = False


class DropdownMenu:
    def __init__(self, screen, pos):
        self.screen = screen
        self.visible = False
        self.dropdown_bg = main.GameStates.SpaceUI.GridClasses.DropDownRect((317, self.screen.get_size()[1] * 0.90), (0, 155, 100), pos)
        self.grid = ShipEditGrids(self.screen, 3, 6, (pos[0]+4, pos[1]+4), 101)
        self.dropdownObjectList = pygame.sprite.Group()
        self.itemList = pygame.sprite.Group()
        self.gridList = self.grid.create_grid()
        self.dropdownObjectList.add(self.dropdown_bg)

    def update(self):
        pass

    def render(self):
        if self.visible:
            self.dropdownObjectList.draw(self.screen)









