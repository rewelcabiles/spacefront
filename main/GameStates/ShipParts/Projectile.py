import pygame
import BasicComponents
import math

#Turn this into sprite cass... or something
class LightBullet(pygame.sprite.Sprite):
    def __init__(self, rotation, center):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 12
        self.maxLifeSpan = 300
        self.curLifeSpan = 0
        self.rotation = rotation
        self.Masterimage = pygame.image.load("assets/images/ship/projectiles/bullet.png").convert_alpha()
        self.image = self.Masterimage.copy()
        self.rect = self.image.get_rect()

        self.initialize(center)

    def initialize(self, center):
        self.image = pygame.transform.rotate(self.Masterimage, self.rotation)
        self.rect.center = center

    def update(self):
        newPos = self.radOfset()
        self.rect.x -= newPos[0]
        self.rect.y -= newPos[1]

        if self.curLifeSpan >= self.maxLifeSpan:
            self.kill()
            print "ded"
        else:
            self.curLifeSpan +=1
    def radOfset(self):
        y = math.cos(math.radians(self.rotation)) * self.velocity
        x = math.sin(math.radians(self.rotation)) * self.velocity
        return [x, y]


class LightLaser(pygame.sprite.Sprite):
    def __init__(self, rotation, center):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 6
        self.maxLifeSpan = 50
        self.curLifeSpan = 0
        self.rotation = rotation
        self.Masterimage = pygame.image.load("assets/images/ship/projectiles/laser.png").convert_alpha()
        self.image = self.Masterimage.copy()
        self.rect = self.image.get_rect()
        self.initialize(center)

    def initialize(self, center):
        self.image = pygame.transform.rotate(self.Masterimage, self.rotation)
        self.rect.center = center

    def update(self):
        newPos = self.radOfset()
        self.rect.x -= newPos[0]
        self.rect.y -= newPos[1]

        if self.curLifeSpan >= self.maxLifeSpan:
            self.kill()
        else:
            self.curLifeSpan +=1

    def radOfset(self):
        y = math.cos(math.radians(self.rotation)) * self.velocity
        x = math.sin(math.radians(self.rotation)) * self.velocity
        return [x, y]