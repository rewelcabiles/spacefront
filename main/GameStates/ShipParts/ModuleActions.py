class FireGun:
    def __init__(self, parent):
        self.parent = parent

    def FireGun(self):
        if self.parent.curWait < self.parent.maxWait:
            self.parent.curWait += 1
        elif self.parent.curWait >= self.parent.maxWait:
            newBullet = self.parent.ammo(self.parent.parent.stats["rotation"], self.parent.Sprite.rect.center)
            self.parent.ChildSprites.add(newBullet)
            self.parent.curWait = 0