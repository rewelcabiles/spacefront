import pygame
import BasicComponents
import weaponParts

class FederationStandard:
    def __init__(self):
        self.name = "Federation Standard"
        self.Sprite = BasicComponents.Sprite("assets/images/ship/hull/Small_FedStandard.png")
        self.Sprite.rect.x = 400
        self.Sprite.rect.y = 400
        self.AllSpriteComponents = pygame.sprite.Group()
        self.AllSpriteComponents.add(self.Sprite)
        self.MaxHealth = 200
        self.CurHealth = 200
        self.Health = BasicComponents.HealthControl((self.MaxHealth, self.CurHealth))
        self.stats = {
            "maxspeed": 440,
            "acceleration": 86,
            "Maxhealth": 100,
            "Curhealth": 100,
            "shield": 50,
            "velocity": 0,
            "rotation": 0,
            "rotspeed": 1
        }
        self.Slots = {
            "Slot1": weaponParts.SmallGunner(self),
            "Slot2": weaponParts.SmallLaser(self)
        }
        self.SlotManager = BasicComponents.ModuleSlots(self.Slots)

        for i in self.Slots:
            if self.Slots[i] is not None:
                self.AllSpriteComponents.add(self.Slots[i].Sprite)
                self.AllSpriteComponents.add(self.Slots[i].ChildSprites)

    def update(self, states):
        self.SlotManager.update()
        self.SlotManager.action(states)
        for i in self.Slots:
            if self.Slots[i] is not None:
                self.AllSpriteComponents.add(self.Slots[i].ChildSprites)

class CruiseLump:
    def __init__(self):
        self.name = "CruiseLump"
        self.Sprite = BasicComponents.Sprite("assets/images/ship/hull/Small_Lump.png")
        self.stats = {
            "maxspeed": 240,
            "acceleration": 6,
            "Maxhealth": 100,
            "Curhealth": 100,
            "shield": 50,
            "velocity": 0,
            "rotation": 0,
            "rotspeed": 1
        }
        self.Slots = {
            "Slot1": weaponParts.SmallLaser(self),
            "Slot2": None
        }
        self.SlotManager = BasicComponents.ModuleSlots(self.Slots)

    def update(self, states):
        self.SlotManager.update()
        self.SlotManager.action(states)