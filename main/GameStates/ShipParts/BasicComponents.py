import pygame
import math


class Sprite(pygame.sprite.Sprite):
    def __init__(self, image):
        pygame.sprite.Sprite.__init__(self)
        self.Masterimage = pygame.image.load(image).convert_alpha()
        self.image = self.Masterimage.copy()
        self.rect = self.image.get_rect()
        self.imageCenter = self.rect.width / 2, self.rect.height / 2

#CHANGE DIS, TOO MESSY MESSY
class Rotate:
    def __init__(self, ship):
        self.Sprite = ship

    def update(self, dt):
        newpos = self.radOfset()
        self.Sprite.Sprite.rect.x += newpos[0] * dt
        self.Sprite.Sprite.rect.y += newpos[1] * dt
        self.rotateUpdate(dt)

    def rotateUpdate(self, dt):
        oldrect = self.Sprite.Sprite.rect.center
        self.Sprite.Sprite.image = pygame.transform.rotate(self.Sprite.Sprite.Masterimage, self.Sprite.stats["rotation"])
        self.Sprite.Sprite.rect = self.Sprite.Sprite.image.get_rect()
        self.Sprite.Sprite.rect.center = oldrect

    def radOfset(self):
        y = math.cos(math.radians(self.Sprite.stats["rotation"])) * self.Sprite.stats["velocity"]
        x = math.sin(math.radians(self.Sprite.stats["rotation"])) * self.Sprite.stats["velocity"]
        return [x, y]


class Movement:
    def __init__(self, stats):
        self.stats = stats

    def update(self, state):
        if state["forward"]:
            self.stats["velocity"] -= self.stats["acceleration"]
        elif state["back"]:
            self.stats["velocity"] += self.stats["acceleration"]
        else:
            if self.stats["velocity"] < 0:
                self.stats["velocity"] += self.stats["acceleration"]
                if self.stats["velocity"] >= 0:
                    self.stats["velocity"] = 0.0
            else:
                self.stats["velocity"] -= self.stats["acceleration"]
                if self.stats["velocity"] <= 0:
                    self.stats["velocity"] = 0.0

        if state["left"]:
            self.stats["rotation"] += self.stats["rotspeed"]
            if self.stats["rotation"] >= 360:
                self.stats["rotation"] = 0
        elif state["right"]:
            self.stats["rotation"] -= self.stats["rotspeed"]
            if self.stats["rotation"] < 0:
                self.stats["rotation"] = 359

        if self.stats["velocity"] >= self.stats["maxspeed"]:
            self.stats["velocity"] = self.stats["maxspeed"]
        elif self.stats["velocity"] <= self.stats["maxspeed"] - (self.stats["maxspeed"] * 2):
            self.stats["velocity"] = self.stats["maxspeed"] - (self.stats["maxspeed"] * 2)


class HealthControl:
    def __init__(self, parentHealth):
        self.maxHealth = parentHealth[0]
        self.curHealth = parentHealth[1]

    def take_damage(self, damage):
        self.curHealth -= damage


class ModuleUpdate:
    def __init__(self, module):
        self.module = module

    def update(self):
        oldrect = self.module.Sprite.rect.center
        self.module.Sprite.image = pygame.transform.rotate(self.module.Sprite.Masterimage, self.module.parent.stats["rotation"])
        self.module.Sprite.rect = self.module.Sprite.image.get_rect()
        self.module.Sprite.rect.center = self.module.parent.Sprite.rect.center

    def radOfset(self):
        y = math.cos(math.radians(self.module.rotation)) * self.module.parent.stats["velocity"]
        x = math.sin(math.radians(self.module.rotation)) * self.module.parent.stats["velocity"]
        return [x, y]


class ModuleSlots:
    def __init__(self, slotdict):
        self.SlotList = slotdict
        self.active_module = None

    def update(self):
        for i in self.SlotList:
            if self.SlotList[i] is not None:
                self.SlotList[i].update()

    def action(self, states):
        if states["Slot1"]:
            if self.SlotList["Slot1"] is None:
                pass
            else:
                self.active_module = self.SlotList["Slot1"]

        if states["Slot2"]:
            if self.SlotList["Slot2"] is None:
                pass
            else:
                self.active_module = self.SlotList["Slot2"]

        if states["click"]:
            if self.active_module is None:
                pass
            else:
                self.active_module.action()
