import pygame
import BasicComponents
import Projectile
import ModuleActions


class SmallGunner:
    def __init__(self, parent):
        self.parent = parent
        self.Sprite = BasicComponents.Sprite("assets/images/ship/weapons/smallGunner.png")
        self.Sprite.icon = pygame.image.load("assets/images/icons/smallGunnerIcon.png").convert_alpha()
        self.ammo = Projectile.LightBullet
        self.Updater = BasicComponents.ModuleUpdate(self)
        self.ChildSprites = pygame.sprite.Group()
        self.maxWait = 50
        self.curWait = 0
        self.ActionModule = ModuleActions.FireGun(self)

    def action(self):
        self.ActionModule.FireGun()

    def update(self):
        self.ChildSprites.update()
        self.Updater.update()


class SmallLaser:
    def __init__(self, parent):
        self.parent = parent
        self.Sprite = BasicComponents.Sprite("assets/images/ship/weapons/smallLaser.png")
        self.Sprite.icon = pygame.image.load("assets/images/icons/smallLaserIcon.png").convert_alpha()
        self.ammo = Projectile.LightLaser
        self.Updater = BasicComponents.ModuleUpdate(self)
        self.ChildSprites = pygame.sprite.Group()
        self.maxWait = 50
        self.curWait = 0
        self.ActionModule = ModuleActions.FireGun(self)

    def action(self):
        self.ActionModule.FireGun()

    def update(self):
        self.ChildSprites.update()
        self.Updater.update()
