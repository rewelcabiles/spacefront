import pygame

from PlayerClasses import Player
from SpaceUI import SpaceHud


class OuterSpace:
    def __init__(self, screen):
        self.screen = screen
        self.player = Player.Player()
        self.background = (pygame.image.load("assets/images/backdrops/space_paralax/black.png").convert(), (0, 0))
        self.renderList = []
        self.AllSpriteList = pygame.sprite.Group()
        self.renderList.append(self.background)
        #self.cargo_menu = GuiElements.CargoMenu(self.screen)
        self.spaceShip = self.player.Ship
        self.SpaceHud = SpaceHud.SpaceHud(self.screen, self.player)

        self.cameraRect = pygame.sprite.Sprite()
        self.cameraRect.image = pygame.Surface((100,100))
        self.cameraRect.rect = self.cameraRect.image.get_rect()
        self.camera = Camera(screen)

        self.random = pygame.sprite.Sprite()
        self.random.image = pygame.image.load("assets\images\ship\hull/red.png")
        self.random.rect = self.random.image.get_rect()
        self.random.rect.x = 400
        self.random.rect.y = 400

        self.AllSpriteList.add(self.random)
        self.AllSpriteList.add(self.spaceShip.ShipPartImages)

    def update(self, dt):
        self.AllSpriteList.add(self.spaceShip.ShipPartImages)
        self.spaceShip.update(dt)
        self.SpaceHud.update()
        self.camera.update(self.cameraRect)
        self.cameraRect.rect.x, self.cameraRect.rect.y = self.spaceShip.Hull.Sprite.rect.center

    def render(self):

        #self.screen.blit(self.background[0], self.background[1])
        for renderObjects in self.renderList:
            self.screen.blit(renderObjects[0], renderObjects[1])
        #self.cargo_menu.render()
        for e in self.AllSpriteList:
            self.screen.blit(e.image, self.camera.apply(e))
        self.SpaceHud.render()

    def handle_events(self, events):
        #self.cargo_menu.handle_events(events)
        self.spaceShip.events(events)


class Camera:
    def __init__(self, screen):
        self.screen = screen
        self.state = pygame.Rect((0, 0), (screen.get_size()))

    def simple_camera(self, cams, target_rect):
        l, t, _, _ = target_rect  # l = left,  t = top
        _, _, w, h = cams  # w = width, h = height
        return pygame.Rect(-l + self.screen.get_width() / 2, -t + self.screen.get_height() / 2, w, h)

    def apply(self, target):
        return target.rect.move(self.state.topleft)

    def update(self, target):
        self.state = self.simple_camera(self.state, target.rect)
